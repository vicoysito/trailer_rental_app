import 'package:provider/provider.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_web_plugins/url_strategy.dart';
import 'package:firebase_core/firebase_core.dart';
import 'auth/firebase_auth/firebase_user_provider.dart';
import 'auth/firebase_auth/auth_util.dart';

import 'backend/push_notifications/push_notifications_util.dart';
import 'backend/firebase/firebase_config.dart';
import 'flutter_flow/flutter_flow_theme.dart';
import 'flutter_flow/flutter_flow_util.dart';
import 'flutter_flow/internationalization.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'flutter_flow/nav/nav.dart';
import 'index.dart';

import 'backend/stripe/payment_manager.dart';

BitmapDescriptor customMarker0 = BitmapDescriptor.defaultMarker;
BitmapDescriptor customMarker1 = BitmapDescriptor.defaultMarker;
BitmapDescriptor customMarker2 = BitmapDescriptor.defaultMarker;
BitmapDescriptor customMarker3 = BitmapDescriptor.defaultMarker;
BitmapDescriptor customMarker4 = BitmapDescriptor.defaultMarker;
BitmapDescriptor customMarker5 = BitmapDescriptor.defaultMarker;
BitmapDescriptor customMarker6 = BitmapDescriptor.defaultMarker;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  usePathUrlStrategy();
  await initFirebase();

  final appState = FFAppState(); // Initialize FFAppState
  await appState.initializePersistedState();

  await initializeStripe();

  customMarker0 = await BitmapDescriptor.fromAssetImage(
      const ImageConfiguration(), 'assets/images/marker0.png');
  customMarker1 = await BitmapDescriptor.fromAssetImage(
      const ImageConfiguration(), 'assets/images/marker1.png');
  customMarker2 = await BitmapDescriptor.fromAssetImage(
      const ImageConfiguration(), 'assets/images/marker2.png');
  customMarker3 = await BitmapDescriptor.fromAssetImage(
      const ImageConfiguration(), 'assets/images/marker3.png');
  customMarker4 = await BitmapDescriptor.fromAssetImage(
      const ImageConfiguration(), 'assets/images/marker4.png');
  customMarker5 = await BitmapDescriptor.fromAssetImage(
      const ImageConfiguration(), 'assets/images/marker5.png');
  customMarker6 = await BitmapDescriptor.fromAssetImage(
      const ImageConfiguration(), 'assets/images/marker6.png');

  runApp(ChangeNotifierProvider(
    create: (context) => appState,
    child: MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  State<MyApp> createState() => _MyAppState();

  static _MyAppState of(BuildContext context) =>
      context.findAncestorStateOfType<_MyAppState>()!;
}

class _MyAppState extends State<MyApp> {
  Locale? _locale;
  ThemeMode _themeMode = ThemeMode.system;

  late Stream<BaseAuthUser> userStream;

  late AppStateNotifier _appStateNotifier;
  late GoRouter _router;

  final authUserSub = authenticatedUserStream.listen((_) {});
  final fcmTokenSub = fcmTokenUserStream.listen((_) {});

  @override
  void initState() {
    super.initState();
    _appStateNotifier = AppStateNotifier.instance;
    _router = createRouter(_appStateNotifier);
    userStream = moovalotFirebaseUserStream()
      ..listen((user) => _appStateNotifier.update(user));
    jwtTokenStream.listen((_) {});
    Future.delayed(
      Duration(milliseconds: 1000),
      () => _appStateNotifier.stopShowingSplashImage(),
    );
  }

  @override
  void dispose() {
    authUserSub.cancel();
    fcmTokenSub.cancel();
    super.dispose();
  }

  void setLocale(String language) {
    setState(() => _locale = createLocale(language));
  }

  void setThemeMode(ThemeMode mode) => setState(() {
        _themeMode = mode;
      });

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: 'Moovalot',
      localizationsDelegates: [
        FFLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      locale: _locale,
      supportedLocales: const [Locale('en', '')],
      theme: ThemeData(brightness: Brightness.light),
      themeMode: _themeMode,
      routerConfig: _router,
    );
  }
}
