// Export pages
export '/pages/login/login_widget.dart' show LoginWidget;
export '/pages/create_account/create_account_widget.dart'
    show CreateAccountWidget;
export '/pages/create_user/create_user_widget.dart' show CreateUserWidget;
export '/pages/forgot_password/forgot_password_widget.dart'
    show ForgotPasswordWidget;
export '/pages/car_driving_parked/car_driving_parked_widget.dart'
    show CarDrivingParkedWidget;
export '/pages/services/services_widget.dart' show ServicesWidget;
export '/pages/payments/payments_widget.dart' show PaymentsWidget;
export '/pages/profile_page/profile_page_widget.dart' show ProfilePageWidget;
export '/pages/edit_profile/edit_profile_widget.dart' show EditProfileWidget;
export '/pages/change_password/change_password_widget.dart'
    show ChangePasswordWidget;
export '/pages/payment_complete/payment_complete_widget.dart'
    show PaymentCompleteWidget;
export '/pages/appointment_details/appointment_details_widget.dart'
    show AppointmentDetailsWidget;
export '/pages/detail_page/detail_page_widget.dart' show DetailPageWidget;
export '/pages/home_map/home_map_widget.dart' show HomeMapWidget;
