import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';

Future initFirebase() async {
  if (kIsWeb) {
    await Firebase.initializeApp(
        options: FirebaseOptions(
            apiKey: "AIzaSyBPBPCRfHOy3teaHHXFBIj2GxmkTIF3reQ",
            authDomain: "neural-cathode-389602.firebaseapp.com",
            projectId: "neural-cathode-389602",
            storageBucket: "neural-cathode-389602.appspot.com",
            messagingSenderId: "229466050072",
            appId: "1:229466050072:web:dd5bdc11693eb40d007d6d",
            measurementId: "G-50GKV4XC1L"));
  } else {
    await Firebase.initializeApp();
  }
}
