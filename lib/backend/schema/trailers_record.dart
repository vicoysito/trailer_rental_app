import 'dart:async';

import 'package:collection/collection.dart';

import '/backend/schema/util/firestore_util.dart';
import '/backend/schema/util/schema_util.dart';

import 'index.dart';
import '/flutter_flow/flutter_flow_util.dart';

class TrailersRecord extends FirestoreRecord {
  TrailersRecord._(
    DocumentReference reference,
    Map<String, dynamic> data,
  ) : super(reference, data) {
    _initializeFields();
  }

  // "phone" field.
  String? _phone;
  String get phone => _phone ?? '';
  bool hasPhone() => _phone != null;

  // "imei" field.
  String? _imei;
  String get imei => _imei ?? '';
  bool hasImei() => _imei != null;

  void _initializeFields() {
    _phone = snapshotData['phone'] as String?;
    _imei = snapshotData['imei'] as String?;
  }

  static CollectionReference get collection =>
      FirebaseFirestore.instance.collection('trailers');

  static Stream<TrailersRecord> getDocument(DocumentReference ref) =>
      ref.snapshots().map((s) => TrailersRecord.fromSnapshot(s));

  static Future<TrailersRecord> getDocumentOnce(DocumentReference ref) =>
      ref.get().then((s) => TrailersRecord.fromSnapshot(s));

  static TrailersRecord fromSnapshot(DocumentSnapshot snapshot) =>
      TrailersRecord._(
        snapshot.reference,
        mapFromFirestore(snapshot.data() as Map<String, dynamic>),
      );

  static TrailersRecord getDocumentFromData(
    Map<String, dynamic> data,
    DocumentReference reference,
  ) =>
      TrailersRecord._(reference, mapFromFirestore(data));

  @override
  String toString() =>
      'TrailersRecord(reference: ${reference.path}, data: $snapshotData)';

  @override
  int get hashCode => reference.path.hashCode;

  @override
  bool operator ==(other) =>
      other is TrailersRecord &&
      reference.path.hashCode == other.reference.path.hashCode;
}

Map<String, dynamic> createTrailersRecordData({
  String? phone,
  String? imei,
}) {
  final firestoreData = mapToFirestore(
    <String, dynamic>{
      'phone': phone,
      'imei': imei,
    }.withoutNulls,
  );

  return firestoreData;
}

class TrailersRecordDocumentEquality implements Equality<TrailersRecord> {
  const TrailersRecordDocumentEquality();

  @override
  bool equals(TrailersRecord? e1, TrailersRecord? e2) {
    return e1?.phone == e2?.phone && e1?.imei == e2?.imei;
  }

  @override
  int hash(TrailersRecord? e) => const ListEquality().hash([e?.phone, e?.imei]);

  @override
  bool isValidKey(Object? o) => o is TrailersRecord;
}
