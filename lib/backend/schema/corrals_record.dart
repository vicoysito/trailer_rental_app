import 'dart:async';

import 'package:collection/collection.dart';

import '/backend/schema/util/firestore_util.dart';
import '/backend/schema/util/schema_util.dart';

import 'index.dart';
import '/flutter_flow/flutter_flow_util.dart';

class CorralsRecord extends FirestoreRecord {
  CorralsRecord._(
    DocumentReference reference,
    Map<String, dynamic> data,
  ) : super(reference, data) {
    _initializeFields();
  }

  // "name" field.
  String? _name;
  String get name => _name ?? '';
  bool hasName() => _name != null;

  // "address" field.
  String? _address;
  String get address => _address ?? '';
  bool hasAddress() => _address != null;

  // "cars" field.
  int? _cars;
  int get cars => _cars ?? 0;
  bool hasCars() => _cars != null;

  // "latlong" field.
  LatLng? _latlong;
  LatLng? get latlong => _latlong;
  bool hasLatlong() => _latlong != null;

  // "trailers" field.
  int? _trailers;
  int get trailers => _trailers ?? 0;
  bool hasTrailers() => _trailers != null;

  // "state" field.
  String? _state;
  String get state => _state ?? '';
  bool hasState() => _state != null;

  // "trailersIds" field.
  List<DocumentReference>? _trailersIds;
  List<DocumentReference> get trailersIds => _trailersIds ?? const [];
  bool hasTrailersIds() => _trailersIds != null;

  void _initializeFields() {
    _name = snapshotData['name'] as String?;
    _address = snapshotData['address'] as String?;
    _cars = castToType<int>(snapshotData['cars']);
    _latlong = snapshotData['latlong'] as LatLng?;
    _trailers = castToType<int>(snapshotData['trailers']);
    _state = snapshotData['state'] as String?;
    _trailersIds = getDataList(snapshotData['trailersIds']);
  }

  static CollectionReference get collection =>
      FirebaseFirestore.instance.collection('corrals');

  static Stream<CorralsRecord> getDocument(DocumentReference ref) =>
      ref.snapshots().map((s) => CorralsRecord.fromSnapshot(s));

  static Future<CorralsRecord> getDocumentOnce(DocumentReference ref) =>
      ref.get().then((s) => CorralsRecord.fromSnapshot(s));

  static CorralsRecord fromSnapshot(DocumentSnapshot snapshot) =>
      CorralsRecord._(
        snapshot.reference,
        mapFromFirestore(snapshot.data() as Map<String, dynamic>),
      );

  static CorralsRecord getDocumentFromData(
    Map<String, dynamic> data,
    DocumentReference reference,
  ) =>
      CorralsRecord._(reference, mapFromFirestore(data));

  @override
  String toString() =>
      'CorralsRecord(reference: ${reference.path}, data: $snapshotData)';

  @override
  int get hashCode => reference.path.hashCode;

  @override
  bool operator ==(other) =>
      other is CorralsRecord &&
      reference.path.hashCode == other.reference.path.hashCode;
}

Map<String, dynamic> createCorralsRecordData({
  String? name,
  String? address,
  int? cars,
  LatLng? latlong,
  int? trailers,
  String? state,
}) {
  final firestoreData = mapToFirestore(
    <String, dynamic>{
      'name': name,
      'address': address,
      'cars': cars,
      'latlong': latlong,
      'trailers': trailers,
      'state': state,
    }.withoutNulls,
  );

  return firestoreData;
}

class CorralsRecordDocumentEquality implements Equality<CorralsRecord> {
  const CorralsRecordDocumentEquality();

  @override
  bool equals(CorralsRecord? e1, CorralsRecord? e2) {
    const listEquality = ListEquality();
    return e1?.name == e2?.name &&
        e1?.address == e2?.address &&
        e1?.cars == e2?.cars &&
        e1?.latlong == e2?.latlong &&
        e1?.trailers == e2?.trailers &&
        e1?.state == e2?.state &&
        listEquality.equals(e1?.trailersIds, e2?.trailersIds);
  }

  @override
  int hash(CorralsRecord? e) => const ListEquality().hash([
        e?.name,
        e?.address,
        e?.cars,
        e?.latlong,
        e?.trailers,
        e?.state,
        e?.trailersIds
      ]);

  @override
  bool isValidKey(Object? o) => o is CorralsRecord;
}
