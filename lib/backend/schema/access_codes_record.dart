import 'dart:async';

import 'package:collection/collection.dart';

import '/backend/schema/util/firestore_util.dart';
import '/backend/schema/util/schema_util.dart';

import 'index.dart';
import '/flutter_flow/flutter_flow_util.dart';

class AccessCodesRecord extends FirestoreRecord {
  AccessCodesRecord._(
    DocumentReference reference,
    Map<String, dynamic> data,
  ) : super(reference, data) {
    _initializeFields();
  }

  // "fri" field.
  List<int>? _fri;
  List<int> get fri => _fri ?? const [];
  bool hasFri() => _fri != null;

  // "mon" field.
  List<int>? _mon;
  List<int> get mon => _mon ?? const [];
  bool hasMon() => _mon != null;

  // "sat" field.
  List<int>? _sat;
  List<int> get sat => _sat ?? const [];
  bool hasSat() => _sat != null;

  // "sun" field.
  List<int>? _sun;
  List<int> get sun => _sun ?? const [];
  bool hasSun() => _sun != null;

  // "thu" field.
  List<int>? _thu;
  List<int> get thu => _thu ?? const [];
  bool hasThu() => _thu != null;

  // "tue" field.
  List<int>? _tue;
  List<int> get tue => _tue ?? const [];
  bool hasTue() => _tue != null;

  // "wed" field.
  List<int>? _wed;
  List<int> get wed => _wed ?? const [];
  bool hasWed() => _wed != null;

  void _initializeFields() {
    _fri = getDataList(snapshotData['fri']);
    _mon = getDataList(snapshotData['mon']);
    _sat = getDataList(snapshotData['sat']);
    _sun = getDataList(snapshotData['sun']);
    _thu = getDataList(snapshotData['thu']);
    _tue = getDataList(snapshotData['tue']);
    _wed = getDataList(snapshotData['wed']);
  }

  static CollectionReference get collection =>
      FirebaseFirestore.instance.collection('access_codes');

  static Stream<AccessCodesRecord> getDocument(DocumentReference ref) =>
      ref.snapshots().map((s) => AccessCodesRecord.fromSnapshot(s));

  static Future<AccessCodesRecord> getDocumentOnce(DocumentReference ref) =>
      ref.get().then((s) => AccessCodesRecord.fromSnapshot(s));

  static AccessCodesRecord fromSnapshot(DocumentSnapshot snapshot) =>
      AccessCodesRecord._(
        snapshot.reference,
        mapFromFirestore(snapshot.data() as Map<String, dynamic>),
      );

  static AccessCodesRecord getDocumentFromData(
    Map<String, dynamic> data,
    DocumentReference reference,
  ) =>
      AccessCodesRecord._(reference, mapFromFirestore(data));

  @override
  String toString() =>
      'AccessCodesRecord(reference: ${reference.path}, data: $snapshotData)';

  @override
  int get hashCode => reference.path.hashCode;

  @override
  bool operator ==(other) =>
      other is AccessCodesRecord &&
      reference.path.hashCode == other.reference.path.hashCode;
}

Map<String, dynamic> createAccessCodesRecordData() {
  final firestoreData = mapToFirestore(
    <String, dynamic>{}.withoutNulls,
  );

  return firestoreData;
}

class AccessCodesRecordDocumentEquality implements Equality<AccessCodesRecord> {
  const AccessCodesRecordDocumentEquality();

  @override
  bool equals(AccessCodesRecord? e1, AccessCodesRecord? e2) {
    const listEquality = ListEquality();
    return listEquality.equals(e1?.fri, e2?.fri) &&
        listEquality.equals(e1?.mon, e2?.mon) &&
        listEquality.equals(e1?.sat, e2?.sat) &&
        listEquality.equals(e1?.sun, e2?.sun) &&
        listEquality.equals(e1?.thu, e2?.thu) &&
        listEquality.equals(e1?.tue, e2?.tue) &&
        listEquality.equals(e1?.wed, e2?.wed);
  }

  @override
  int hash(AccessCodesRecord? e) => const ListEquality()
      .hash([e?.fri, e?.mon, e?.sat, e?.sun, e?.thu, e?.tue, e?.wed]);

  @override
  bool isValidKey(Object? o) => o is AccessCodesRecord;
}
