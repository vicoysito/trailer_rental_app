import 'package:flutter/material.dart';
import '/backend/backend.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'flutter_flow/flutter_flow_util.dart';

class FFAppState extends ChangeNotifier {
  static final FFAppState _instance = FFAppState._internal();

  factory FFAppState() {
    return _instance;
  }

  FFAppState._internal();

  Future initializePersistedState() async {}

  void update(VoidCallback callback) {
    callback();
    notifyListeners();
  }

  int _currentNumberPickerValue = 0;
  int get currentNumberPickerValue => _currentNumberPickerValue;
  set currentNumberPickerValue(int _value) {
    _currentNumberPickerValue = _value;
  }

  String _timeRented = '0:00';
  String get timeRented => _timeRented;
  set timeRented(String _value) {
    _timeRented = _value;
  }

  String _paymentrent = '0.00';
  String get paymentrent => _paymentrent;
  set paymentrent(String _value) {
    _paymentrent = _value;
  }

  String _finalPricePerHour = '14.00';
  String get finalPricePerHour => _finalPricePerHour;
  set finalPricePerHour(String _value) {
    _finalPricePerHour = _value;
  }

  String _finalPricePerMinute = '3.50';
  String get finalPricePerMinute => _finalPricePerMinute;
  set finalPricePerMinute(String _value) {
    _finalPricePerMinute = _value;
  }

  int _finalPricePerHourInteger = 0;
  int get finalPricePerHourInteger => _finalPricePerHourInteger;
  set finalPricePerHourInteger(int _value) {
    _finalPricePerHourInteger = _value;
  }
}

LatLng? _latLngFromString(String? val) {
  if (val == null) {
    return null;
  }
  final split = val.split(',');
  final lat = double.parse(split.first);
  final lng = double.parse(split.last);
  return LatLng(lat, lng);
}

void _safeInit(Function() initializeField) {
  try {
    initializeField();
  } catch (_) {}
}

Future _safeInitAsync(Function() initializeField) async {
  try {
    await initializeField();
  } catch (_) {}
}
