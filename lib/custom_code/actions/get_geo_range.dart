// Automatic FlutterFlow imports
import '/backend/backend.dart';
import '/flutter_flow/flutter_flow_theme.dart';
import '/flutter_flow/flutter_flow_util.dart';
import 'index.dart'; // Imports other custom actions
import 'package:flutter/material.dart';
// Begin custom action code
// DO NOT REMOVE OR MODIFY THE CODE ABOVE!

import 'index.dart'; // Imports other custom actions

import 'package:georange/georange.dart';

GeoRange georange = GeoRange();
final _firestore = FirebaseFirestore.instance;

Future<List<CorralsRecord>> getGeoRange(
  String distance,
  LatLng location,
) async {
  //Parse kilometers to Miles
  double distanceParsed = double.parse(distance) * 1609.34;
  int distanceInt = distanceParsed.round();
  Range range = georange.geohashRange(location.latitude, location.longitude,
      distance: distanceInt);

  var corralsSnapshot = await FirebaseFirestore.instance
      .collection("corrals")
      .where("geohash", isGreaterThanOrEqualTo: range.lower)
      .where("geohash", isLessThanOrEqualTo: range.upper)
      .get();

  // Create a list of CorralsRecord objects from the query results
  List<CorralsRecord> records = corralsSnapshot.docs
      .map((doc) => CorralsRecord.fromSnapshot(doc))
      .toList();

  print(corralsSnapshot.size);
  return records;
}
