// Automatic FlutterFlow imports
import '/backend/backend.dart';
import '/flutter_flow/flutter_flow_theme.dart';
import '/flutter_flow/flutter_flow_util.dart';
import 'index.dart'; // Imports other custom widgets
import '/custom_code/actions/index.dart'; // Imports custom actions
import 'package:flutter/material.dart';
// Begin custom widget code
// DO NOT REMOVE OR MODIFY THE CODE ABOVE!

import 'package:numberpicker/numberpicker.dart';

class HourPickerWidget extends StatefulWidget {
  const HourPickerWidget(
      {Key? key, this.width, this.height, required this.refreshPageUI})
      : super(key: key);

  final double? width;
  final double? height;
  final Future<dynamic> Function() refreshPageUI;

  @override
  _HourPickerWidgetState createState() => _HourPickerWidgetState();
}

class _HourPickerWidgetState extends State<HourPickerWidget> {
  late int _selectedValueHours;
  late int _selectedValueMin;
  late String _selectedValueHoursFinal;

  final List<double> PRICE_HOURS = [14.00, 13.00, 12.00, 11.00, 10.00];
  final List<double> PRICE_MINUTE = [3.50, 3.25, 3.00, 2.75, 2.50];

  final int MAX_HOURS = 72;
  final int MAX_MIN = 45;

  @override
  void initState() {
    super.initState();
    _selectedValueHoursFinal = "0";
    _selectedValueHours = 0;
    _selectedValueMin = 0;
  }

  void increaseHour() {
    if (_selectedValueHours == 24) return;
    setState(() {
      _selectedValueHours = _selectedValueHours + 1;
      _selectedValueHoursFinal =
          _selectedValueHours.toString() + ":" + _selectedValueMin.toString();
      FFAppState().timeRented = _selectedValueHoursFinal;
      setStateTime(_selectedValueHoursFinal);
      widget.refreshPageUI();
    });
  }

  int getMinutes(String time) {
    // given String get minutes format 1:1
    List<String> timeList = time.split(':');
    int minutes = (int.parse(timeList[0]) * 60) + int.parse(timeList[1]);
    return minutes;
  }

  double getPricePerHour(int minutes) {
    /*Price per hour
     $14.00 
     $13.00 
     $12.00 
     $11.00 
     $10.00 
     Price per 15 minute
      $3.50 
      $3.25 
      $3.00 
      $2.75 
      $2.50 
    */
    double priceToCharge = 0.00;
    switch (minutes) {
      case < 59:
        priceToCharge = PRICE_MINUTE.elementAt(0);
        break;
      case < 119:
        priceToCharge = PRICE_MINUTE.elementAt(1);
        break;
      case < 179:
        priceToCharge = PRICE_MINUTE.elementAt(2);
        break;
      case < 239:
        priceToCharge = PRICE_MINUTE.elementAt(3);
        break;
      case >= 240:
        priceToCharge = PRICE_MINUTE.elementAt(4);
        break;
      default:
        return 0;
    }
    return priceToCharge;
  }

  void setStateTime(String time) {
    String finalPricePerHour = PRICE_HOURS.elementAt(0).toStringAsFixed(2);
    String finalPricePerMin = PRICE_MINUTE.elementAt(0).toStringAsFixed(2);
    int minutes = getMinutes(time);
    double pricePerHour = getPricePerHour(minutes);
    //Minutes per Price divided by quarter and finally by total of hours
    List<String> totalOfHoursList = time.split(":");
    int totalOfHours = int.parse(totalOfHoursList[0]);
    double price = (minutes * pricePerHour) / 15;

    if (totalOfHours < 5) {
      finalPricePerHour =
          PRICE_HOURS.elementAt(totalOfHours).toStringAsFixed(2);
      finalPricePerMin =
          PRICE_MINUTE.elementAt(totalOfHours).toStringAsFixed(2);
    } else {
      finalPricePerHour = PRICE_HOURS.elementAt(4).toStringAsFixed(2);
      finalPricePerMin = PRICE_MINUTE.elementAt(4).toStringAsFixed(2);
    }

    FFAppState().finalPricePerHour = finalPricePerHour;
    FFAppState().finalPricePerMinute = finalPricePerMin;
    FFAppState().paymentrent = price.toStringAsFixed(2);
    FFAppState().finalPricePerHourInteger = int.parse(price.toString().replaceAll(".","")+"0");

  }

  void decreaseHour() {
    setState(() {
      if (_selectedValueHours == 0) return;
      _selectedValueHours = _selectedValueHours - 1;
      _selectedValueHoursFinal =
          _selectedValueHours.toString() + ":" + _selectedValueMin.toString();
      FFAppState().timeRented = _selectedValueHoursFinal;
      setStateTime(_selectedValueHoursFinal);
      widget.refreshPageUI();
    });
    FFAppState().timeRented = _selectedValueHoursFinal;
    setStateTime(_selectedValueHoursFinal);
    widget.refreshPageUI();
  }

  void setTotalHours() {
    _selectedValueHoursFinal =
        _selectedValueHours.toString() + ":" + _selectedValueMin.toString();
    FFAppState().timeRented = _selectedValueHoursFinal;
    setStateTime(_selectedValueHoursFinal);
    widget.refreshPageUI();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 411,
      height: 120,
      decoration: BoxDecoration(
        color: FlutterFlowTheme.of(context).secondaryBackground,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            width: MediaQuery.sizeOf(context).width * 0.1,
            height: 120,
            decoration: BoxDecoration(
              color: FlutterFlowTheme.of(context).secondaryBackground,
            ),
            child: InkWell(
              splashColor: Colors.transparent,
              focusColor: Colors.transparent,
              hoverColor: Colors.transparent,
              highlightColor: Colors.transparent,
              onTap: () async {
                decreaseHour();
              },
              child: Icon(
                Icons.minimize,
                color: FlutterFlowTheme.of(context).secondaryText,
                size: 24,
              ),
            ),
          ),
          Container(
            width: MediaQuery.sizeOf(context).width * 0.4,
            height: 120,
            child: NumberPicker(
                value: _selectedValueHours,
                minValue: 0,
                maxValue: MAX_HOURS,
                step: 1,
                onChanged: (valuehours) {
                  setState(() {
                    _selectedValueHours = valuehours;
                    _selectedValueHoursFinal = _selectedValueHours.toString() +
                        ":" +
                        _selectedValueMin.toString();
                    FFAppState().timeRented = _selectedValueHoursFinal;
                    setStateTime(_selectedValueHoursFinal);
                    widget.refreshPageUI();
                  });
                }),
          ),
          InkWell(
            splashColor: Colors.transparent,
            focusColor: Colors.transparent,
            hoverColor: Colors.transparent,
            highlightColor: Colors.transparent,
            onTap: () async {
              increaseHour();
            },
            child: NumberPicker(
                value: _selectedValueMin,
                minValue: 0,
                maxValue: MAX_MIN,
                step: 15,
                onChanged: (valueMin) {
                  setState(() {
                    _selectedValueMin = valueMin;
                    _selectedValueHoursFinal = _selectedValueHours.toString() +
                        ":" +
                        _selectedValueMin.toString();
                    FFAppState().timeRented = _selectedValueHoursFinal;
                    setStateTime(_selectedValueHoursFinal);
                    widget.refreshPageUI();
                  });
                }),
          ),
          Container(
            width: MediaQuery.sizeOf(context).width * 0.1,
            height: 120,
            decoration: BoxDecoration(
              color: FlutterFlowTheme.of(context).secondaryBackground,
            ),
            child: InkWell(
              splashColor: Colors.transparent,
              focusColor: Colors.transparent,
              hoverColor: Colors.transparent,
              highlightColor: Colors.transparent,
              onTap: () async {
                increaseHour();
              },
              child: Icon(
                Icons.add,
                color: FlutterFlowTheme.of(context).secondaryText,
                size: 24,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
