// Automatic FlutterFlow imports
import '/backend/backend.dart';
import '/flutter_flow/flutter_flow_theme.dart';
import '/flutter_flow/flutter_flow_util.dart';
import 'index.dart'; // Imports other custom widgets
import '/custom_code/actions/index.dart'; // Imports custom actions
import 'package:flutter/material.dart';
// Begin custom widget code
// DO NOT REMOVE OR MODIFY THE CODE ABOVE!

import 'package:numberpicker/numberpicker.dart';

class PickerWidget extends StatefulWidget {
  const PickerWidget({
    Key? key,
    this.width,
    this.height,
    required this.startValue,
    required this.endValue,
    required this.stepValue,
    required this.currentValue,
    required this.refreshPageUI,
  }) : super(key: key);

  final double? width;
  final double? height;
  final int startValue;
  final int endValue;
  final int stepValue;
  final int currentValue;
  final Future<dynamic> Function() refreshPageUI;

  @override
  _PickerWidgetState createState() => _PickerWidgetState();
}

class _PickerWidgetState extends State<PickerWidget> {
  late int _selectedValue;

  @override
  void initState() {
    super.initState();
    _selectedValue = widget.currentValue;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: widget.width,
        height: widget.height,
        child: NumberPicker(
            value: _selectedValue,
            minValue: widget.startValue,
            maxValue: widget.endValue,
            step: widget.stepValue,
            onChanged: (value) {
              setState(() => _selectedValue = value);
              FFAppState().currentNumberPickerValue = _selectedValue;
              widget.refreshPageUI();
            }));
  }
}
